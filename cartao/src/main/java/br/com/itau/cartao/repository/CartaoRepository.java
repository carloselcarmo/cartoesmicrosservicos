package br.com.itau.cartao.repository;

import br.com.itau.cartao.model.Cartao;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartaoRepository extends JpaRepository<Cartao,Integer >
{
    Cartao findByNumero(String numero);
}
