package br.com.itau.cartao.controller;

import br.com.itau.cartao.DTO.CartaoEntrada;
import br.com.itau.cartao.DTO.CartaoSaida;
import br.com.itau.cartao.DTO.CartaoSemStatus;
import br.com.itau.cartao.DTO.CartaoStatus;
import br.com.itau.cartao.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController
{
    @Autowired
    CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoSaida criar(@RequestBody @Valid CartaoEntrada cartao)
    {
        System.out.println(System.currentTimeMillis() + " - Criaram o cartão " + cartao.getNumero() + " do cliente " + cartao.getClienteId());
        return  cartaoService.criar(cartao);
    }

    @PatchMapping("/{numero}")
    public CartaoSaida alterarStatusAtivo(@PathVariable(name = "numero") String numero, @RequestBody @Valid CartaoStatus cartaoStatusDTO)
    {
        System.out.println(System.currentTimeMillis() + " - Alteraram o status do cartão  " + numero + " para status " + cartaoStatusDTO.getAtivo());
        return cartaoService.alterarStatus(numero, cartaoStatusDTO);
    }

    @PostMapping("/expirar/{id}")
    public CartaoSaida expirar(@PathVariable(name = "id") Integer id)
    {
        System.out.println(System.currentTimeMillis() + " - Expiraram o cartão " + id);
        return cartaoService.expirar(id);
    }

    @GetMapping("/{numero}")
    public CartaoSemStatus buscarPorNumero(@PathVariable(name = "numero") String numero)
    {
        System.out.println(System.currentTimeMillis() + " - Buscaram o cartão " + numero);
        return cartaoService.buscarCartaoSemStausPorNumero(numero);
    }

    @GetMapping("/buscarPorId/{Id}")
    public CartaoSaida buscarPorId(@PathVariable(name = "Id") Integer id)
    {
        System.out.println(System.currentTimeMillis() + " - Buscaram o cartão " + id);
        return cartaoService.buscarCartaoSaidaPorId(id);
    }
}
