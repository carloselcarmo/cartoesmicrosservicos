package br.com.itau.cartao.DTO;


import br.com.itau.cartao.model.Cartao;

public class CartaoSemStatus
{
    private Integer id;

    private String numero;

    private Integer clienteId;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public CartaoSemStatus() {
    }

    public CartaoSemStatus(Cartao cartao)
    {
        this.setClienteId(cartao.getClienteId());
        this.setId(cartao.getId());
        this.setNumero(cartao.getNumero());
    }
}
