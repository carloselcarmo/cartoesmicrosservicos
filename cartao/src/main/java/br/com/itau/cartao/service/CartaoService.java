package br.com.itau.cartao.service;

import br.com.itau.cartao.DTO.CartaoEntrada;
import br.com.itau.cartao.DTO.CartaoSaida;
import br.com.itau.cartao.DTO.CartaoSemStatus;
import br.com.itau.cartao.DTO.CartaoStatus;
import br.com.itau.cartao.client.Cliente;
import br.com.itau.cartao.client.ClienteClient;
import br.com.itau.cartao.exception.CartaoNaoEncontradoException;
import br.com.itau.cartao.exception.NumeroCartaoJaUtilizadoException;
import br.com.itau.cartao.model.Cartao;
import br.com.itau.cartao.repository.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService
{
    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteClient clienteClient;

    public CartaoSaida criar(CartaoEntrada cartaoDTO)
    {
        Cliente cliente;

        cliente = clienteClient.buscarPorId(cartaoDTO.getClienteId());

        Cartao cartaoDb = cartaoRepository.findByNumero(cartaoDTO.getNumero());

        if(cartaoDb != null)
        {
            throw new NumeroCartaoJaUtilizadoException();
        }
        else
        {
            Cartao cartao = new Cartao();
            cartao.setClienteId(cartaoDTO.getClienteId());
            cartao.setNumero(cartaoDTO.getNumero());
            cartao.setAtivo(false);

            cartao = cartaoRepository.save(cartao);

            return new CartaoSaida(cartao);
        }
    }

    public Cartao buscarPorId(Integer id)
    {
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(id);

        if(cartaoOptional.isPresent())
        {
            Cartao cartao = cartaoOptional.get();
            return cartao;
        }
        else
        {
            throw new CartaoNaoEncontradoException();
        }
    }

    public CartaoSaida buscarCartaoSaidaPorId(Integer id)
    {
        return new CartaoSaida(buscarPorId(id));
    }

    private Cartao buscarPorNumero(String numero)
    {
        Cartao cartao = cartaoRepository.findByNumero(numero);

        if(cartao == null)
        {
            throw new CartaoNaoEncontradoException();
        }

        return cartao;
    }

    public CartaoSemStatus buscarCartaoSemStausPorNumero(String numero)
    {
        return new CartaoSemStatus(buscarPorNumero(numero));
    }

    public CartaoSaida alterarStatus(String numero, CartaoStatus cartaoStatusDTO)
    {
        Cartao cartao = buscarPorNumero(numero);

        return salvarStatus(cartao, cartaoStatusDTO.getAtivo());
    }

    public CartaoSaida expirar(Integer id)
    {
        Cartao cartao = buscarPorId(id);

        return salvarStatus(cartao, false);
    }

    private CartaoSaida salvarStatus(Cartao cartao, Boolean ativo)
    {
        cartao.setAtivo(ativo);

        cartao = cartaoRepository.save(cartao);

        return new CartaoSaida(cartao);
    }
}
