package br.com.itau.cartao.client;

import br.com.itau.cartao.exception.ClienteNaoEncontradoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteCodeDecoder implements ErrorDecoder
{
    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response)
    {
        if(response.status() == 404)
        {
            return new ClienteNaoEncontradoException();
        }
        return errorDecoder.decode(s, response);
    }
}
