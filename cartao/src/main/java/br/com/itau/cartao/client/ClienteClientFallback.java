package br.com.itau.cartao.client;

import br.com.itau.cartao.exception.APIClienteNaoDisponivelException;

public class ClienteClientFallback implements ClienteClient {
    @Override
    public Cliente buscarPorId(Integer idCliente) {
        throw new APIClienteNaoDisponivelException();
    }
}
