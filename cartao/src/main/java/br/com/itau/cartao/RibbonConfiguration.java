package br.com.itau.cartao;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import com.netflix.loadbalancer.RoundRobinRule;
import com.netflix.loadbalancer.WeightedResponseTimeRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfiguration
{
    @Bean
    public IRule getRule()
    {
        //return new RandomRule();
        //return new RoundRobinRule(); //Default
        return new WeightedResponseTimeRule();
    }
}
