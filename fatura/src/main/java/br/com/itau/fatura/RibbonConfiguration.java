package br.com.itau.fatura;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.WeightedResponseTimeRule;
import org.springframework.context.annotation.Bean;

public class RibbonConfiguration
{
    @Bean
    public IRule getRule()
    {
        //return new RandomRule();
        //return new RoundRobinRule(); //Default
        return new WeightedResponseTimeRule();
    }
}