package br.com.itau.fatura.client;

import br.com.itau.fatura.exception.APICartaoNaoDisponivelException;
import com.netflix.client.ClientException;

public class CartaoClientLoadBalancerFallback implements CartaoClient
{
    private Exception ex;

    public CartaoClientLoadBalancerFallback(Exception ex)
    {
        this.ex = ex;
    }

    @Override
    public Cartao buscarPorId(Integer idCartao) {
        if(ex.getCause() instanceof ClientException)
        {
            throw new APICartaoNaoDisponivelException();
        }
        throw (RuntimeException) ex;
    }

    @Override
    public void expirar(Integer idCartao) {
        if(ex.getCause() instanceof ClientException)
        {
            throw new APICartaoNaoDisponivelException();
        }
        throw (RuntimeException) ex;
    }
}