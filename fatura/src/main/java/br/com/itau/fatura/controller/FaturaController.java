package br.com.itau.fatura.controller;

import br.com.itau.fatura.DTO.CartaoStatusSaida;
import br.com.itau.fatura.DTO.PagamentoSaida;
import br.com.itau.fatura.DTO.ValorPago;
import br.com.itau.fatura.service.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class FaturaController
{
    @Autowired
    FaturaService faturaService;

    @GetMapping("/fatura/{cliente_id}/{cartao_id}")
    public List<PagamentoSaida> buscarPorIdClienteId_cartao(@PathVariable(name = "cartao_id") int id_cartao, @PathVariable(name = "cliente_id") int id_cliente)
    {
        System.out.println(System.currentTimeMillis() + " - Buscaram a fatura - Cliente: " + id_cliente + " Cartão: " + id_cartao);
        return faturaService.buscarPorIdClienteId_cartao(id_cartao, id_cliente);
    }

    @PostMapping("/fatura/{cliente_id}/{cartao_id}/pagar")
    public ValorPago pagar(@PathVariable(name = "cartao_id") int id_cartao, @PathVariable(name = "cliente_id") int id_cliente)
    {
        System.out.println(System.currentTimeMillis() + " - Pagaram a fatura Cliente: " + id_cliente + " Cartão: " + id_cartao);
        return faturaService.pagar(id_cartao, id_cliente);
    }

    @PostMapping("/fatura/{cliente_id}/{cartao_id}/expirar")
    public CartaoStatusSaida expirar(@PathVariable(name = "cartao_id") int id_cartao, @PathVariable(name = "cliente_id") int id_cliente)
    {
        System.out.println(System.currentTimeMillis() + " - Expiraram a fatura Cliente: " + id_cliente + " Cartão: " + id_cartao);
        return faturaService.expirar(id_cartao, id_cliente);
    }
}
