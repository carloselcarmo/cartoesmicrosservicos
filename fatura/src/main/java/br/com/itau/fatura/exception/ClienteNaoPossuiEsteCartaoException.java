package br.com.itau.fatura.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.FAILED_DEPENDENCY, reason = "Este cliente não possui o cartão informado")
public class ClienteNaoPossuiEsteCartaoException extends RuntimeException {
}
