package br.com.itau.fatura.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "PAGAMENTO")
public interface PagamentoClient
{
    @GetMapping("/pagamentos/{idCartao}")
    List<Pagamento> buscarPorId_cartao(@PathVariable Integer idCartao);

    @DeleteMapping("/{id}")
    void excluir(@PathVariable Integer id);
}
