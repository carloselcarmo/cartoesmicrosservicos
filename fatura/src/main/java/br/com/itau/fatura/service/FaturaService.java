package br.com.itau.fatura.service;

import br.com.itau.fatura.DTO.CartaoStatusSaida;
import br.com.itau.fatura.DTO.PagamentoSaida;
import br.com.itau.fatura.DTO.ValorPago;
import br.com.itau.fatura.client.*;
import br.com.itau.fatura.exception.ClienteNaoPossuiEsteCartaoException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class FaturaService
{
    @Autowired
    private CartaoClient cartaoClient;

    @Autowired
    private ClienteClient clienteClient;

    @Autowired
    private PagamentoClient pagamentoClient;

    private Cliente buscarCliente(int idCliente)
    {
        return clienteClient.buscarPorId(idCliente);
    }

    private Cartao buscarCartao(int idCartao)
    {
        return cartaoClient.buscarPorId(idCartao);
    }

    private List<Pagamento> buscarPagamentos(Integer idCartao)
    {
        return pagamentoClient.buscarPorId_cartao(idCartao);
    }

    public List<PagamentoSaida> buscarPorIdClienteId_cartao(int id_cartao, int id_cliente)
    {
        validarCartao(id_cliente, id_cartao);

        List<Pagamento> lista = buscarPagamentos(id_cartao);

        List<PagamentoSaida> extrato = new ArrayList<>();

        for(Pagamento pagamento : lista)
        {
            extrato.add(new PagamentoSaida(pagamento));
        }

        return extrato;
    }

    public ValorPago pagar(int id_cartao, int id_cliente)
    {
        validarCartao(id_cliente, id_cartao);

        List<Pagamento> lista = buscarPagamentos(id_cartao);

        BigDecimal valor = BigDecimal.valueOf(0);

        for(Pagamento pagamento : lista)
        {
            valor = valor.add(BigDecimal.valueOf(pagamento.getValor()));
            pagamentoClient.excluir(pagamento.getId());
        }

        ValorPago valorPagoDTO = new ValorPago();
        valorPagoDTO.setId(id_cartao);
        valorPagoDTO.setPagoEm(LocalDate.now());
        valorPagoDTO.setValorPago(valor.setScale(2, RoundingMode.HALF_EVEN).doubleValue());

        return valorPagoDTO;
    }

    public CartaoStatusSaida expirar(int id_cartao, int id_cliente)
    {
        validarCartao(id_cliente, id_cartao);

        cartaoClient.expirar(id_cartao);

        CartaoStatusSaida cartaoStatusSaidaDTO = new CartaoStatusSaida();
        cartaoStatusSaidaDTO.setStatus("ok");

        return cartaoStatusSaidaDTO;
    }

    private void validarCartao(Integer id_cliente, Integer id_cartao)
    {
        Cliente cliente = buscarCliente(id_cliente);

        Cartao cartao = buscarCartao(id_cartao);

        if(!cliente.getId().equals(cartao.getClienteId()))
            throw new ClienteNaoPossuiEsteCartaoException();
    }
}
