package br.com.itau.fatura.DTO;

import br.com.itau.fatura.client.Pagamento;

public class PagamentoSaida
{
    private Integer id;

    private Integer cartaoId;

    private String descricao;

    private Double valor;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(Integer cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public PagamentoSaida() {
    }

    public PagamentoSaida(Pagamento pagamento)
    {
        setId(pagamento.getId());
        setCartaoId(pagamento.getCartaoId());
        setDescricao(pagamento.getDescricao());
        setValor(pagamento.getValor());
    }
}
