package br.com.itau.fatura.client;

import br.com.itau.fatura.exception.APIClienteNaoDisponivelException;
import com.netflix.client.ClientException;

public class ClienteClientLoadBalancerFallback implements ClienteClient
{
    private Exception ex;

    public ClienteClientLoadBalancerFallback(Exception ex)
    {
        this.ex = ex;
    }

    @Override
    public Cliente buscarPorId(Integer idCliente)
    {
        if(ex.getCause() instanceof ClientException)
        {
            throw new APIClienteNaoDisponivelException();
        }
        throw (RuntimeException) ex;
    }
}