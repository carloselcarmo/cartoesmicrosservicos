package br.com.itau.fatura.client;

import br.com.itau.fatura.exception.APICartaoNaoDisponivelException;

public class CartaoClientFallback implements CartaoClient {
    @Override
    public Cartao buscarPorId(Integer idCartao)
    {
        throw new APICartaoNaoDisponivelException();
    }

    @Override
    public void expirar(Integer idCartao)
    {
        throw new APICartaoNaoDisponivelException();
    }
}
