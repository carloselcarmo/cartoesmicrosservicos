package br.com.itau.fatura.client;

import br.com.itau.fatura.exception.APIClienteNaoDisponivelException;

public class ClienteClientFallback implements ClienteClient {
    @Override
    public Cliente buscarPorId(Integer idCliente) {
        throw new APIClienteNaoDisponivelException();
    }
}