package br.com.itau.fatura.DTO;

import javax.validation.constraints.NotNull;

public class CartaoStatusSaida
{
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public CartaoStatusSaida() {
    }
}
