package br.com.itau.fatura.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.BAD_GATEWAY, reason = "A API de Cartões não está disponível")
public class APICartaoNaoDisponivelException extends RuntimeException   {
}