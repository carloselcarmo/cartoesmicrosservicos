package br.com.itau.fatura.client;

import br.com.itau.fatura.exception.APIPagamentoNaoDisponivelException;

import java.util.List;

public class PagamentoClientFallBack implements PagamentoClient {
    @Override
    public List<Pagamento> buscarPorId_cartao(Integer idCartao) {
        throw new APIPagamentoNaoDisponivelException();
    }

    @Override
    public void excluir(Integer id) {
        throw new APIPagamentoNaoDisponivelException();
    }
}
