package br.com.itau.fatura.client;

import br.com.itau.fatura.exception.APIPagamentoNaoDisponivelException;
import com.netflix.client.ClientException;

import java.util.List;

public class PagamentoClientLoadBalancerFallback implements PagamentoClient
{
    private Exception ex;

    public PagamentoClientLoadBalancerFallback(Exception ex)
    {
        this.ex = ex;
    }

    @Override
    public List<Pagamento> buscarPorId_cartao(Integer idCartao) {
        if(ex.getCause() instanceof ClientException)
        {
            throw new APIPagamentoNaoDisponivelException();
        }
        throw (RuntimeException) ex;
    }

    @Override
    public void excluir(Integer id) {
        if(ex.getCause() instanceof ClientException)
        {
            throw new APIPagamentoNaoDisponivelException();
        }
        throw (RuntimeException) ex;
    }
}
