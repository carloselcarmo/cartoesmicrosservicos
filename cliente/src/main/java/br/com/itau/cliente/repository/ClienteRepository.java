package br.com.itau.cliente.repository;

import br.com.itau.cliente.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente,Integer > {
}
