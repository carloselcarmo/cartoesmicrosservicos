package br.com.itau.pagamento.service;

import br.com.itau.pagamento.DTO.*;
import br.com.itau.pagamento.client.Cartao;
import br.com.itau.pagamento.client.CartaoClient;
import br.com.itau.pagamento.client.Cliente;
import br.com.itau.pagamento.client.ClienteClient;
import br.com.itau.pagamento.exception.CartaoInativoException;
import br.com.itau.pagamento.model.Pagamento;
import br.com.itau.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService
{
    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;

    @Autowired
    private ClienteClient clienteClient;

    public PagamentoSaida criar(PagamentoEntrada pagamentoDTO)
    {
        Cartao cartao = buscarCartao(pagamentoDTO.getCartao_id());

         if(cartao.getAtivo())
         {
             Pagamento pagamento = new Pagamento();
             pagamento.setCartaoId(pagamentoDTO.getCartao_id());
             pagamento.setDescricao(pagamentoDTO.getDescricao());
             pagamento.setValor(pagamentoDTO.getValor());

             pagamento = pagamentoRepository.save(pagamento);

             return new PagamentoSaida(pagamento);
         }
         else
         {
             throw new CartaoInativoException();
         }
    }

    public List<PagamentoSaida> buscarPorCartaoId(Integer id)
    {
        Cartao cartao = buscarCartao(id);

        List<Pagamento> lista = pagamentoRepository.findByCartaoId(id);

        List<PagamentoSaida> extrato = new ArrayList<>();

        for(Pagamento pagamento : lista)
        {
            extrato.add(new PagamentoSaida(pagamento));
        }

        return extrato;
    }

    private Cliente buscarCliente(Integer idCliente)
    {
        return clienteClient.buscarPorId(idCliente);
    }

    private Cartao buscarCartao(Integer idCartao)
    {
        return cartaoClient.buscarPorId(idCartao);
    }

    public void excluir(Integer id)
    {
        pagamentoRepository.deleteById(id);
    }
}
