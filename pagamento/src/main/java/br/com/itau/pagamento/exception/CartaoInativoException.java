package br.com.itau.pagamento.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.FAILED_DEPENDENCY, reason = "Cartão inativo")
public class CartaoInativoException extends RuntimeException  {
}
