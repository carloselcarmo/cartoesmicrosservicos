package br.com.itau.pagamento.client;

import br.com.itau.pagamento.exception.CartaoNaoEncontradoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoCodeDecoder implements ErrorDecoder
{
    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response)
    {
        if(response.status() == 404)
        {
            return new CartaoNaoEncontradoException();
        }
        return errorDecoder.decode(s, response);
    }
}
