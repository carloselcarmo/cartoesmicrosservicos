package br.com.itau.pagamento.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code= HttpStatus.BAD_GATEWAY, reason = "A API de Clientes não está disponível")
public class APIClienteNaoDisponivelException extends RuntimeException  {
}