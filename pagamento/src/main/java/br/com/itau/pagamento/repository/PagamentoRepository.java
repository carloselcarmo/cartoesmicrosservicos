package br.com.itau.pagamento.repository;
import br.com.itau.pagamento.model.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PagamentoRepository extends JpaRepository<Pagamento,Integer >
{
    List<Pagamento> findByCartaoId(Integer cartaoId);
}
