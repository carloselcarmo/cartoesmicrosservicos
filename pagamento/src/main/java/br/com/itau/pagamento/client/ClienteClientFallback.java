package br.com.itau.pagamento.client;

import br.com.itau.pagamento.exception.APIClienteNaoDisponivelException;

public class ClienteClientFallback implements ClienteClient {
    @Override
    public Cliente buscarPorId(Integer idCliente) {
        throw new APIClienteNaoDisponivelException();
    }
}
