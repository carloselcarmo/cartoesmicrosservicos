package br.com.itau.pagamento.client;

import br.com.itau.pagamento.exception.ClienteNaoEncontradoException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteCodeDecoder implements ErrorDecoder
{
    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response)
    {
        if(response.status() == 404)
        {
            return new ClienteNaoEncontradoException();
        }
        return errorDecoder.decode(s, response);
    }
}
