package br.com.itau.pagamento.client;

import br.com.itau.pagamento.exception.APICartaoNaoDisponivelException;

public class CartaoClientFallback implements CartaoClient
{
    @Override
    public Cartao buscarPorId(Integer idCartao)
    {
        throw new APICartaoNaoDisponivelException();
    }

    @Override
    public void expirar(Integer idCartao)
    {
        throw new APICartaoNaoDisponivelException();
    }
}
