package br.com.itau.pagamento.controller;

import br.com.itau.pagamento.DTO.PagamentoEntrada;
import br.com.itau.pagamento.DTO.PagamentoSaida;
import br.com.itau.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PagamentoController
{
    @Autowired
    PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoSaida criar(@RequestBody @Valid PagamentoEntrada pagamento)
    {
        System.out.println(System.currentTimeMillis() + " - Criaram o pagamento " + pagamento.getDescricao());
        return  pagamentoService.criar(pagamento);
    }

    @GetMapping("/pagamentos/{id_cartao}")
    public List<PagamentoSaida> buscarPorId_cartao(@PathVariable(name = "id_cartao") int id_cartao)
    {
        System.out.println(System.currentTimeMillis() + " - Buscaram o pagamento pelo id_Cartao " + id_cartao);
        return pagamentoService.buscarPorCartaoId(id_cartao);
    }

    @DeleteMapping("/{id}")
    public void excluir(@PathVariable(name = "id")  Integer id)
    {
        System.out.println(System.currentTimeMillis() + " - Excluíram o pagamento " + id);
        pagamentoService.excluir(id);
    }
}
