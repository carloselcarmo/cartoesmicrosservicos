package br.com.itau.pagamento.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "CARTAO", configuration = CartaoClientConfiguration.class)
public interface CartaoClient
{
    @GetMapping("/cartao/buscarPorId/{idCartao}")
    Cartao buscarPorId(@PathVariable Integer idCartao);

    @PostMapping("/cartao/expirar/{idCartao}")
    void expirar(@PathVariable Integer idCartao);
}
